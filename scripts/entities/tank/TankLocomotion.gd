class_name TankLocomotion
extends KinematicBody

var Pool = load("res://addons/godot-object-pool/pool.gd");

export var tank_brain: Resource = TankBrainBase.new();

export var turret_rate_of_turn: float = 10;
export var hull_rate_of_turn: float = 10;
export var hull_max_allowed_steering_angle: float = 5;
export var max_speed: float = 10;
export var acceleration: float = 20;

onready var turret_root: Spatial = $TurretRoot;
onready var body_root: Spatial = $BodyRoot;

var tank_brain_internal: TankBrainBase;
var bullet_pool;

var velocity := Vector3();
var velocity_direction := Vector3();
var velocity_magnitude: float = 0;

# Called when the node enters the scene tree for the first time.
func _ready():
	tank_brain_internal = tank_brain
	if not tank_brain_internal:
		printerr(name, " did not have a tank brain attached, you're going to get a braindead tank!");
		tank_brain_internal = TankBrainBase.new();
	
	bullet_pool = Pool.new(tank_brain_internal.number_of_bullets_available, "bullet_" + str(self.get_instance_id()) + "_", tank_brain_internal.bullet_template);
	bullet_pool.call_deferred("add_to_node", get_node("../"));
	
	tank_brain_internal.initialize(self);

func _process(delta):
	process_locomotion(delta);
	process_turret(delta);
	process_fire_control(delta);
	
func process_locomotion(delta):
	var input_direction_vector := tank_brain_internal.determine_driving_direction(delta, body_root);
	
	if (input_direction_vector.is_equal_approx(Vector3.ZERO)):
		return;

	var hull_forward_local := -body_root.transform.basis.z;
	var facing_difference := (hull_forward_local.signed_angle_to(input_direction_vector, Vector3.UP));
	var hull_max_allowed_steering_angle_rads = deg2rad(hull_max_allowed_steering_angle);
	# var steering_utilization_ratio = facing_difference / hull_max_allowed_steering_angle_rads;
	
	if (abs(facing_difference) < hull_max_allowed_steering_angle_rads):
		
		velocity += input_direction_vector * acceleration * delta;
		velocity_direction = velocity.normalized();
		velocity_magnitude = velocity.length();
		
		if (velocity_magnitude >= max_speed):
			velocity = velocity_direction * max_speed;
		
		velocity = move_and_slide(velocity, Vector3.UP);
		velocity_direction = velocity.normalized();
		velocity_magnitude = velocity.length();
		body_root.rotate(Vector3.UP, (-body_root.transform.basis.z).signed_angle_to(input_direction_vector, Vector3.UP) * hull_rate_of_turn * delta);
		
	else:
		var turn_accel = hull_rate_of_turn * delta;
		if (abs(facing_difference) < turn_accel):
			body_root.rotate(Vector3.UP, facing_difference);
		else:
			body_root.rotate(Vector3.UP, turn_accel * sign(facing_difference));
		velocity = lerp(velocity, Vector3.ZERO, 2*acceleration * delta);
		
		velocity = move_and_slide(velocity, Vector3.UP);

func process_turret(delta):
	var turret_desired_direction := tank_brain_internal.determine_turrent_direction(delta, turret_root);
	
	var turret_forward := -turret_root.transform.basis.z;
	var angle := turret_forward.signed_angle_to(turret_desired_direction, Vector3.UP);
	
	var turn_distance_rads = min(abs(angle), turret_rate_of_turn * delta) * sign(angle);
	
	if !is_zero_approx(angle):
		turret_root.rotate(Vector3.UP, turn_distance_rads);

func process_fire_control(delta):
	var should_shoot := tank_brain_internal.determine_should_fire(delta, bullet_pool.get_dead_size(), turret_root);
	
	if (should_shoot):
		var bullet_direction = -turret_root.get_global_transform().basis.z
		var bullet_initial_position = get_global_transform().origin + (bullet_direction * 1.1) + Vector3.UP;
		var bullet = bullet_pool.get_first_dead();
		if bullet: bullet.shoot(bullet_initial_position, bullet_direction);
