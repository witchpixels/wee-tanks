class_name PlayerTankBrain extends TankBrainBase

export var fire_delay_seconds: float = 0.5;

var tank_body_node: KinematicBody;
var viewport: Viewport;
var scene_world: World;
var y_baseline: float;

export var recast_timer: float = 0;

func initialize(tank_body: KinematicBody) -> void:
	tank_body_node = tank_body;
	viewport = tank_body.get_viewport();
	scene_world = tank_body.get_world();
	
	y_baseline = tank_body_node.get_global_transform().origin.y;

func determine_driving_direction(_delta: float, _tank_body_transform: Spatial) -> Vector3:
	var input_direction_vector = Vector3(
		Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left"),
		0,
		Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")
	).normalized();

	if (input_direction_vector.is_equal_approx(Vector3.ZERO)):
		return Vector3.ZERO
	
	return input_direction_vector;
	
func determine_turrent_direction(_delta: float, turret_transform: Spatial) -> Vector3:
	var mouse_position = viewport.get_mouse_position();
	var camera = viewport.get_camera();
	var from = camera.project_ray_origin(mouse_position);
	var to = camera.project_ray_normal(mouse_position) * 1000;
	
	var space_state = scene_world.get_direct_space_state();
	var ray_hit: Dictionary = space_state.intersect_ray(from, to);
	
	if !ray_hit.has("position"):
		return -turret_transform.transform.basis.z;
	
	var mouse_3d_position = ray_hit["position"];
	mouse_3d_position.y = y_baseline;
	
	return (mouse_3d_position - turret_transform.get_global_transform().origin).normalized();

func determine_should_fire(_delta: float, _shots_remaining: int, _turret_transform: Spatial) -> bool:
	recast_timer += _delta
	if recast_timer > fire_delay_seconds and Input.is_key_pressed(KEY_SPACE):
		recast_timer = 0;
		return true;
	return false;

