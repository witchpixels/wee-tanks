class_name Bullet extends KinematicBody

export var projectile_velocity: float = 1;
export var ricochet_allowance: int;
export var particle_systems: Array;


onready var collision_shape: CollisionShape = $CollisionShape;
#onready var hit_particle_system: Particles = $HitTuft;
var travel_direction: Vector3 = -transform.basis.z;
var ricochet_count: int = 0;

var dead: bool = true;

signal killed (object)

func shoot(initial_position: Vector3, initial_heading: Vector3) -> void:
	for p in particle_systems:
		if p is Particles: p.restart();
	
	set_global_transform(Transform.IDENTITY.translated(initial_position));
	rotate(Vector3.UP, (-transform.basis.z).signed_angle_to(initial_heading, Vector3.UP));
	
	ricochet_count = 0;
	travel_direction = initial_heading;
	enable();

func _ready():
	disable();
	
func _process(delta: float):
	if dead: return;
	
	var collision := move_and_collide(travel_direction * projectile_velocity * delta);
	
	if collision:
		
		if collision.collider is TankLocomotion:
			var tl := collision.collider as TankLocomotion;
			print("killed due to collision with " + tl.name);
			disable();
		
		#hit_particle_system.restart();
		travel_direction = travel_direction.bounce(collision.normal).normalized();
		ricochet_count += 1
	
	rotate(Vector3.UP, (-transform.basis.z).signed_angle_to(travel_direction, Vector3.UP));
	
	if (ricochet_count >= ricochet_allowance):
		disable();

func disable():
	hide();
	collision_shape.set_disabled(true);
	dead = true;
	emit_signal("killed", self);
	
func enable():
	show();
	collision_shape.set_disabled(false);
	dead = false;
	
