class_name TankBrainBase extends Resource

export var turret_type: String = "square";
export var tank_material_tint: Color;
export var bullet_template: Resource;
export var number_of_bullets_available: int = 5;

func initialize(_tank_body: KinematicBody):
	pass

func determine_driving_direction(_delta: float, _tank_body_transform: Spatial) -> Vector3:
	return Vector3.ZERO;
	
func determine_turrent_direction(_delta: float, _turret_transform: Spatial) -> Vector3:
	return Vector3.FORWARD;

func determine_should_fire(_delta: float, _shots_remaining: int, _turret_transform: Spatial) -> bool:
	return false;
